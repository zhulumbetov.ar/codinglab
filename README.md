The website shows which car components has each car model in a database.

The backend is made by using node.js and express js. Backend connected to the nosql database 'car-database'

The front is simple yet. The basic style is made by using bootstrap. The first outline for the representation
of car components for a specific car based on database is made on React.js

Trello link: 
https://trello.com/b/7K0jdF8A/carstore
